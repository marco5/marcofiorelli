#!/usr/bin/env python2
"""This script reads in csv records and renames files."""
import os
import sys
import shutil

# import unicodecsv as csv
# create dictionary
IDs = {}
olddirectory = ''
newdirectory = ''
first = True
# open and store the csv file
try:
    with open('int.csv', 'r') as f:
        for line in f:
            line = line.rstrip()
            if first:
                olddirectory, newdirectory = line.split(',')
                if not os.path.exists(olddirectory):
                    print 'Old directory not found'
                    sys.exit(1)
                if not os.path.exists(newdirectory):
                    print 'New directory not found'
                    sys.exit(1)
                first = False
                continue
            newint, oldint = line.split(',')
            IDs[newint] = oldint
except IOError:
    print 'File Not Found'
    sys.exit(1)
#
# print IDs
# print olddirectory,newdirectory
#
for key in IDs:
    oldfilename = olddirectory + '/' + key + '.rrd'
    newfilename = newdirectory + '/' + IDs[key] + '.rrd'
    if os.path.isfile(oldfilename):
        print oldfilename, newfilename
        try:
            shutil.copy(oldfilename, newfilename)
        except IOError as e:
            print 'file copy error', oldfilename, e.strerror
            pass
