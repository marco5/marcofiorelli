#!/bin/bash
# greps the device names from the network-devices log and counts their occurrences.
#
echo "...this will take a bit to run ... 5 min."
f="$(mktemp /tmp/chatty.XXXXXX)"
s=`/usr/bin/awk '{print $4}' example_log_file > $f`
echo "temporary output file is $f "
/usr/bin/env python3 count.py $f
echo "removing temporary file $f "
/bin/rm "$f"
