#!/usr/bin/env python3

# This scripts finds the top 10 chattiest hosts which write to syslog-ng by
#  parsing out the 4th field in the log entry and counting them.

import sys
import re
import operator

# This function returns the host name or ip address in the syslog entry line
def findHost(line):
	x = line.split()
	out = (x[3])
	return out

def wordFrequency(w):
	wc = {}
	for word in w:
		if not wc.get(word):
			wc[word] = 1
		else:
			wc[word] += 1
	return wc

name1 = sys.argv[1]
f = open(name1, 'r')

hostlist = []
for line in f:
	host = findHost(line)
	hostlist.append(host)

x = wordFrequency(hostlist)
z = sorted(x.items(), key=operator.itemgetter(1), reverse=False)
# # for i in z:
# # 	print(i)
print(z)
# for i in z:
# 	print("Host: {} | logs: {}".format(i, x[i]))

f.close()


# x = wordFrequency(hostlist)
# print(x)
# print(sorted(x, key=sorting.get)

# for element in sortedResults:
# 		print(sortedResults[element], element)

# d = wordFrequency(grepHost(name1))
# results = sorted(d, key=d.get)
	#except IndexError:
	#	return False

# def readFile(name):
# 	f = open(name)
# 	text = f.read()
# 	f.close()
# 	return text
# x = open('test.log', 'r')
# for line in x:
#   split.line

# results = sorted(d, key=d.get, reverse=True)
# keys = list(d.keys())
# keys = [item.lower() for item in keys]
# keys = list(d.keys())
# keys.sort()

# for element in results:
#	print("Word: {} | Frequency: {}".format(element, d[element]))
