
#!/usr/bin/env python3
import re

INPUT_FILE = """#host,port,input bps,output bps
switch01,te0/1,2378748485,8474857587
switch01,te0/2,43787438,334878745
switch01,te0/3,347374387,3847875847
switch01,te0/4,474784375,37483742347
switch02,te0/1,2323478897,972323
switch02,te0/2,4747843232,37483742347
switch03,te0/1,7128512,3633742347
switch03,te0/2,474784371,3748374923"""

# Considering the above file contents, parse it and ouput the names of the switch and ports in order of highest output bps to lowest


lst = []
for line in INPUT_FILE.split('\n'):
    bps = re.findall(r'bps', line)
    if bps:
        pass
    else:
        x = line.split(',')
        lst.append(x)
y = sorted(lst, key = lambda x: int(x[3]))
for list2 in y:
    print(list2)


