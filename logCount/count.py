#!/usr/bin/env python3

# x = open('test.log', 'r')
# for line in x:
#   split.line


import sys

def readFile(name):
	f = open(name)
	text = f.read()
	f.close()
	return text

# Regex for finding Host name or IP on line
def grepHost(line):
    # out = re.findall( r'[0-9]+(?:\.[0-9]+){3}', line )
	out = re.findall( r'([0-9]+(\.[0-9]+){3})|([a-z]{3}[0-9]+)-([a-z]{2}[0-9]+)', line )
	try:
        out = str(out[0])
        return out
    except IndexError:
        return False

def wordFrequenzy(w):
	lst = w.split()
	wc = {}
	for word in lst:
		if not wc.get(word):
			wc[word] = 1
		else:
			wc[word] += 1
	return wc

name1=sys.argv[1]
print("importing", name1, "into dictionary")
d = wordFrequenzy(readFile(name1))
results = sorted(d, key=d.get)

for element in results:
		print(d[element], element)

# results = sorted(d, key=d.get, reverse=True)
# keys = list(d.keys())
# keys = [item.lower() for item in keys]
# keys = list(d.keys())
# keys.sort()

# for element in results:
#	print("Word: {} | Frequency: {}".format(element, d[element]))
