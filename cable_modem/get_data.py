#!/usr/bin/env python3
import os.path
import datetime
import csv
import requests
from bs4 import BeautifulSoup

def get_values(url): # function to scrape values from a web page, web page is passed in url form
    if url is not None:
        page = requests.get(url)
        soup = BeautifulSoup(page.text, 'html.parser')

        tds = (soup.find_all('td'))

        signal_noise_ratio = [datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")]
        #signal_noise_ratio.append('date_here')
        signal_noise_ratio.append(tds[11].text.strip().split()[0])
        signal_noise_ratio.append(tds[12].text.strip().split()[0])
        signal_noise_ratio.append(tds[13].text.strip().split()[0])
        signal_noise_ratio.append(tds[14].text.strip().split()[0])
        return(signal_noise_ratio)

def WriteListtoCSV(data):
    with open ('testdata.csv', 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL )
        writer.writerow(data)

def main():
    if __name__ == "__main__":
        main()

#print((datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+','), (val[0], val[1], val[2], val[3]))
file_exists = os.path.isfile('testdata.csv')
with open ('testdata.csv', 'a') as csvfile:
    headers = ['Date', 'sig0', 'sig1', 'sig2', 'sig3']
    writer = csv.DictWriter(csvfile, fieldnames=headers)
    if not file_exists:
        writer.writeheader()
WriteListtoCSV(get_values('http://192.168.100.1/cmSignalData.htm'))
